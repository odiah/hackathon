package com.sysomos.core.search.config;

public enum DataAccessConfig {
	
	REXSTER_HOST("172.26.161.75"),
	REXTER_PORT("8182"),

	SQL_URL("jdbc:mysql://si58.sysomos.pvt:3306/tweets?requireSSL=false&useUnicode=true&characterEncoding=UTF-8&jdbcCompliantTruncation=false"),
	SQL_USER("readonly"),
	SQL_PASS("readonlySQL"),

	HBASE_DISTRIBUTED("true"),
	HBASE_QUORUM("10.200.2.11,10.200.2.13,10.200.2.14,10.200.2.15,10.200.2.16"),
	HBASE_PORT("2181"),

	SOLR_BASE_URL_STAGING("http://10.1.86.62:8080/api/rest/v1/"),
	SOLR_BASE_URL_PRODUCT("http://10.200.24.9/api/rest/v1/"),
	SOLR_ACTOR_RESOURCE("actorpersona/get"),
	SOLR_POST_RESOURCE("posts");
	
	public String param;
	
	DataAccessConfig(String param) {
		this.param = param;
	}
	
	@Override
	public String toString() {
		return param;
	}
}
