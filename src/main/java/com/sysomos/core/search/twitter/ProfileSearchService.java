package com.sysomos.core.search.twitter;

import java.util.Collection;
import java.util.List;

import com.sysomos.core.search.transfer.SearchResult;
import com.sysomos.core.search.transfer.ProfileVO;

/**
 * 
 * @author JasonLiu
 *
 */
public interface ProfileSearchService {

	/**
	 * Obtain the {@link ProfileVO} of a collection of twitter ids. Default
	 * fields contained in the resulting VOs are the following.
	 * 
	 * <p>
	 * "name", "id", "screenName", "description", "gender", "pictureUrl",
	 * "profileUrl", "influenceScore", "followerCount", "friendCount",
	 * "postCount"
	 * 
	 * @param userids
	 * @return
	 */
	SearchResult<ProfileVO> search(Collection<Long> userids);

	/**
	 * Obtain {@link ProfileVO} of a collection of twitter ids with specified
	 * fields.
	 * 
	 * @param userids
	 * @param fields
	 * @return
	 */
	SearchResult<ProfileVO> search(Collection<Long> userids, List<String> fields);

}
