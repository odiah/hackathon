package com.sysomos.core.search.twitter.factory;

import com.sysomos.core.search.twitter.TweetSearchService;
import com.sysomos.core.search.twitter.impl.GridTweetsSearchServiceImpl;

/**
 * 
 * @author JasonLiu
 *
 */
public class TweetSearchFactory {

	public static TweetSearchService getService() {
		return new GridTweetsSearchServiceImpl();
	}

}
