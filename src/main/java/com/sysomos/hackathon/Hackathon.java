package com.sysomos.hackathon;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.beust.jcommander.Parameter;
import com.sysomos.hackathon.solr.SOLRTweet;
import com.sysomos.hackathon.utils.Counter;

public class Hackathon {

	private static final long HOUR_TIME_MILLIS = 1 * 60 * 60 * 1000;

	public static class Parameters {
		@Parameter(names = { "-query" }, required = true, description = "Specifies a query or a list "
				+ "of queries", variableArity = true)
		public List<String> queries;
		@Parameter(names = "method", required = true, description = "Method to use to assign sentiment to contents")
		public int method;
		@Parameter(names = "-startDateMillis", required = true, description = "Start Date in milliseconds")
		public Long startDateMillis;
		@Parameter(names = "-endDateMillis", required = true, description = "End Date in milliseconds")
		public Long endDateMillis;
	}

	/**
	 * Process result
	 * 
	 * @param results
	 * @return
	 */
	public List<String> topHashtags(final List<SOLRTweet> results, int top) {
		if (CollectionUtils.isEmpty(results)) {
			return null;
		}
		Counter<String> counterForHashtags = new Counter<String>();
		for (SOLRTweet tweet : results) {
			if (tweet == null || StringUtils.isEmpty(tweet.getContents())) {
				continue;
			}
			String[] hashTags = tweet.getHashTags();
			if (hashTags == null) {
				continue;
			}
			for (String hashtag : hashTags) {
				counterForHashtags.add(hashtag);
			}
		}
		List<String> topHashtags = counterForHashtags.getTop(top);
		return topHashtags;
	}
}
