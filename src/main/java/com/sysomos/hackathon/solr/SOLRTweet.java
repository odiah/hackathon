package com.sysomos.hackathon.solr;

import com.sysomos.core.search.transfer.TweetVO;

public class SOLRTweet extends TweetVO {

	private String[] hashTags;

	public String[] getHashTags() {
		return hashTags;
	}

	public void setHashTags(String[] hashTags) {
		this.hashTags = hashTags;
	}

}
