package com.sysomos.hackathon.solr;

import java.util.List;

import org.joda.time.DateTime;

import com.sysomos.core.search.transfer.SearchResult;
import com.sysomos.core.search.twitter.impl.GridTweetsSearchServiceImpl;

public class QuerySOLR {

	private static final int TIME_WINDOW = 7;

	public List<SOLRTweet> getMatchingTweets(String query) {
		DateTime dt = new DateTime();
		long startDateMillis = dt.minusHours(TIME_WINDOW).getMillis();
		long endDateMillis = dt.getMillis();
		return getMatchingTweets(query, startDateMillis, endDateMillis);
	}

	/**
	 * Output tweets matching a given search query.
	 * 
	 * @param query
	 *            Query
	 * @param startDateMillis
	 *            Start of the time window for searching the query
	 * @param endDateMillis
	 *            End of the time window for searching the query
	 * @return a list of TweetVO that match the search query
	 */
	public List<SOLRTweet> getMatchingTweets(String query,
			long startDateMillis, long endDateMillis) {
		GridTweetsSearchServiceImpl service = new GridTweetsSearchServiceImpl();
		SearchResult<SOLRTweet> searchResult = (SearchResult<SOLRTweet>) service
				.searchRandom(query, startDateMillis, endDateMillis, 10000);
		return searchResult == null ? null : searchResult.getResults();
	}

}
