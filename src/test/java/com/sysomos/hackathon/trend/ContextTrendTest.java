package com.sysomos.hackathon.trend;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import com.beust.jcommander.JCommander;
import com.sysomos.hackathon.exception.TrendException;
import com.sysomos.hackathon.solr.QuerySOLR;
import com.sysomos.hackathon.solr.SOLRTweet;
import com.sysomos.hackathon.utils.Pair;

public class ContextTrendTest {

	private ContextTrend trend;
	private QuerySOLR solr;
	private ContextTrend.Parameters params;
	private static final List<String> ARG_QUERIES = new ArrayList<String>();

	static {

		ARG_QUERIES.addAll(Arrays.asList(new String[] { "-query", "oscars" }));

		ARG_QUERIES.addAll(Arrays.asList(new String[] { "-top", "20" }));

		ARG_QUERIES.addAll(Arrays.asList(new String[] { "-baseline", "6" }));

		ARG_QUERIES.addAll(Arrays.asList(new String[] { "-hour", "22" }));

		ARG_QUERIES.addAll(Arrays.asList(new String[] { "-startDateMillis",
				String.valueOf(1424628000000L) }));

		ARG_QUERIES.addAll(Arrays.asList(new String[] { "-endDateMillis",
				String.valueOf(1424664000000L) }));
	}

	private static final String ARGS[] = ARG_QUERIES
			.toArray(new String[ARG_QUERIES.size()]);
	@Rule
	public TestRule watcher = new TestWatcher() {
		@Override
		protected void starting(Description description) {
			System.out.println("Starting test: " + description.getMethodName());
		}
	};

	@Before
	public void setUp() {
		solr = new QuerySOLR();
		trend = new ContextTrend();
		params = new ContextTrend.Parameters();
		new JCommander(params, ARGS).parse();
	}

	@Test
	public void getMatchingTweets() {
		List<SOLRTweet> matchingTweets = solr.getMatchingTweets(params.query,
				params.startDateMillis, params.endDateMillis);
		if (CollectionUtils.isEmpty(matchingTweets)) {
			System.out.println("No Match Found");
		} else {
			System.out.println("**************************");
			System.out.println("Results: " + matchingTweets.size());
			System.out.println("**************************");
			for (SOLRTweet tweet : matchingTweets) {
				if (tweet == null) {
					continue;
				}
				String[] hashTags = tweet.getHashTags();
				if (hashTags == null) {
					continue;
				}
				String hTTags = "";
				for (int i = 0; i < hashTags.length; i++) {
					if (i < hashTags.length - 1) {
						hTTags += hashTags[i] + ", ";
						continue;
					}
					hTTags += hashTags[i];
				}
				System.out.println("Hashtags: " + hTTags);
			}
			System.out.println("*******************************");
		}
		System.out.println();
		System.out.println("*******************************");
	}

	@Test
	public void getTrendingTopicsTest() {
		try {
			List<Pair<String, Pair<Double, Integer>>> trends = trend
					.getTrendingTopics(params);
			if (CollectionUtils.isEmpty(trends)) {
				System.out.println("*******************************");
				System.out.println("No Trends");
				System.out.println("*******************************");
			} else {
				String hTTags = "";
				for (int i = 0; i < trends.size(); i++) {
					hTTags += trends.get(i).getA().trim() + ": \n"
							+ "    [ Mean: " + trends.get(i).getB().getA()
							+ ", Count: " + trends.get(i).getB().getB()
							+ " ]\n";
				}
				System.out.println("*******************************");
				System.out.println("Trending HashTags: " + trends.size());
				System.out.println("*******************************");
				System.out.println(hTTags);
				System.out.println("*******************************");
			}
		} catch (TrendException e) {
			System.out.println(e.getLocalizedMessage());
			e.printStackTrace();
		}
	}

	@After
	public void tearDown() {
		solr = null;
		trend = null;
		params = null;
	}
}
